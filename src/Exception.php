<?php

namespace Backtheweb\Google;

class Exception extends \Exception
{
    protected $message = 'Google Service Api error';

}
