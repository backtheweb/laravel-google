<?php

namespace Backtheweb\Google\Maps;

use Backtheweb\Google\Contract\GoogleClientContract;
use Backtheweb\Google\Models\Location;

class Timezone extends GoogleClientContract
{

    /** @var string  */
    protected $base_uri = 'https://maps.googleapis.com/maps/api/timezone/json';

    /**
     * @param $lng
     * @param $lat
     * @return mixed
     * @throws \Exception
     */
    public function get($lng, $lat)
    {
        $params = array(
            'location'  => $lat . ',' . $lng,
        );

        $this->standardize($data);

        return $this->call($params);
    }

    public function find($lng, $lat)
    {
        $data = $this->get($lng, $lat);

        return new Maps\Timezone($data);
    }

    public function getByLocation(Location $loc)
    {
        $data = $this->get($loc->lng, $loc->lat);

        return new Maps\Timezone($data);
    }

    public function getId($lng, $lat)
    {
        $data = $this->get($lng, $lat);

        return isset($data->timeZoneId) ? $data->timeZoneId : null;
    }

    protected function standardize(&$data){

        if(null === $data){
            return;
        }

        if(!property_exists($data, 'status')){
            return;
        }

        if($data->status !== 'OK'){
            return;
        }

        if(property_exists($data, 'timeZoneId') && $data->timeZoneId === "Asia/Calcutta") {
            $data->timeZoneId = "Asia/Kolkata";
        } else if(property_exists($data, 'timeZoneId') && $data->timeZoneId === "Asia/Saigon") {
            $data->timeZoneId = "Asia/Ho_Chi_Minh";
        }
    }

}