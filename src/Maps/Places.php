<?php

namespace Backtheweb\Google\Maps;

use Backtheweb\Google\Contract\GoogleClientContract;
use Backtheweb\Google\Model\AddressComponent;
use Backtheweb\Google\Model\GeoCodeAddress;
use Backtheweb\Google\Model\Geometry;

class Places extends GoogleClientContract
{
    const INPUT_TYPE_TEXT   = 'textquery';
    const INPUT_TYPE_PHONE  = 'phonenumber';

    /** @var string  */
    protected $base_uri = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json';

    /** @var bool  */
    public $sensor = false;

    public $region = null;

    public $components = [];

    /**
     * @param $address
     * @return mixed|null
     */
    public function get($value, $region = null)
    {
        $this->region = $region;

        $params = [
            'input'   => $value,
        ];

        $response = $this->call($params);

        return $response;
    }

    public function getLatLng($address)
    {
        $output = $this->get($address);

        return isset($output->results[0]->geometry->location) ? $output->results[0]->geometry->location :  null;
    }

    public function getResult($value, $region = null)
    {
        $response = $this->get($value, $region);

        if(isset($response) && $response->status === static::STATUS_OK){

            return $this->parseResult($response->results[0]);
        }

        return null;
    }

    public function getResults($address, $region = null)
    {
        $output   = [];
        $response = $this->get($address, $region);

        if(isset($response) && $response->status === static::STATUS_OK){

            foreach ($response->results as $item){

                $output[] = $this->parseResult($item);
            }
        }

        return $output;
    }

    public function getGeoCodeAddress($address, $region = null): GeoCodeAddress
    {
        $response = $this->get($address, $region);

        if(!$response){

            throw new \Exception(static::STATUS_RESPONSE_ERROR);
        }

        if($response->status !== Maps::STATUS_OK){

            $this->throwErrorResponse($response);
        }

        return $this->parseResult($response->results[0]);
    }


    public function getLocality($name, $country = null): GeoCodeAddress
    {

        $this->addComponent(AddressComponent::TYPE_LOCALITY, $name);

        if($country) {

            $this->addComponent(AddressComponent::TYPE_COUNTRY, $country);
        }

        $response = $this->call($this->getQuery());

        if(!$response){

            throw new \Exception(static::STATUS_RESPONSE_ERROR);
        }

        if($response->status !== Maps::STATUS_OK){

            throw new \Exception($response->status);
        }

        return $this->parseResult($response->results[0]);
    }

    public function getCountry($name): GeoCodeAddress
    {

        $this->addComponent(AddressComponent::TYPE_COUNTRY, $name);

        $response = $this->call($this->getQuery());

        if(!$response){

            throw new \Exception(static::STATUS_RESPONSE_ERROR);
        }

        if($response->status !== Maps::STATUS_OK){

            throw new \Exception($response->status);
        }

        return $this->parseResult($response->results[0]);
    }

    private function parseResult(\stdClass $item){

        return new GeoCodeAddress([
            'address_components' => (array) $item->address_components,
            'geometry'           => new Geometry((array) $item->geometry),
            'place_id'           => $item->place_id,
            'types'              => (array) $item->types
        ]);
    }

    public function getQuery(Array $params = []) : array
    {
        return array_merge( [
            'key'        => $this->apiKey,
            'language'   => $this->lang,
            'timestamp'  => time(),
            'sensor'     => $this->sensor      ? 'true' : 'false',
            'region'     => $this->region      ? strtolower($this->region)    : null,
            'inputtype'  => 'textquery',

        ], $params);
    }

    public function addComponent($key, $value)
    {
        $this->components[$key] = $value;

        return $this;
    }

    protected function getComponentsString()
    {
        foreach($this->components as $key => $value)
        {
            $x[] = sprintf('%s:%s', $key, $value);
        }

        return join('|', $x);
    }

    /**
     * @return null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param null $region
     */
    public function setRegion($region): void
    {
        $this->region = $region;
    }
}