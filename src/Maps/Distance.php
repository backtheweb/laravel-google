<?php

namespace Backtheweb\Google\Maps;

use Backtheweb\Google\Contract\GoogleClientContract;

class Distance extends GoogleClientContract
{

    const MODE_DRIVING   = 'driving';
    const MODE_WALKING   = 'walking';
    const MODE_BICYCLING = 'bicycling';
    const MODE_TRANSIT   = 'transit';

    const AVOID_TOLLS    = 'tolls';
    const AVOID_HIGHWAYS = 'highways';
    const AVOID_FERRIES  = 'ferries';
    const AVOID_INDOOR   = 'indoor';

    const UNITS_METRIC   = 'metric';
    const UNITS_IMPERIAL = 'imperial';

    const TRAFFIC_MODEL_BEST_GUESS  = 'best_guess';
    const TRAFFIC_MODEL_PESSIMISTIC = 'pessimistic';
    const TRAFFIC_MODEL_OPTIMISTIC  = 'optimistic';

    const TRANSIT_MODE_BUS      = 'bus';
    const TRANSIT_MODE_SUBWAY   = 'subway';
    const TRANSIT_MODE_TRAIN    = 'train';
    const TRANSIT_MODE_TRAM     = 'tram';
    const TRANSIT_MODE_RAIL     = 'rail';  // === train|tram|subway

    const TRANSIT_ROUTING_PREFERENCE_LESS_WALKING    = 'less_walking';
    const TRANSIT_ROUTING_PREFERENCE_FEWER_TRANSFERS = 'fewer_transfers';

    /** @var string  */
    protected $base_uri = 'https://maps.googleapis.com/maps/api/distancematrix/json';

    /**
     * @param $lng
     * @param $lat
     * @return mixed
     * @throws \Exception
     */
    public function get()
    {
        switch( func_num_args() )
        {
            case 2:

                $params['origins'] = func_get_arg(0) . '|' . func_get_arg(1);

                break;

            case 4:

                $params['origins'] = func_get_arg(0) . ',' . func_get_arg(1) . '|' . func_get_arg(2) . ',' . func_get_arg(3);

                break;

            default:

                throw new \Exception('Num arguments not valid');
        }

        return $this->call($params);
    }

    public function setParams(Array $params = [])
    {
        return [
            'mode'                          => null,
            'origins'                       => null,
            'avoid'                         => null,
            'units'                         => null,
            'arrival_time'                  => null,
            'departure_time'                => null,
            'traffic_model'                 => null,
            'transit_mode'                  => null,
            'transit_routing_preference'    => null,
        ];
    }
}