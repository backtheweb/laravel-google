<?php

namespace Backtheweb\Google\Contract;

use GuzzleHttp\Client;
;

/**
 * Class ServiceAbstract
 * @package N\Geo\Service\Google
 */
abstract class GoogleClientContract
{
    const STATUS_OK                 = 'OK';
    const STATUS_ZERO_RESULTS       = 'ZERO_RESULTS';
    const STATUS_OVER_QUERY_LIMIT   = 'OVER_QUERY_LIMIT';
    const STATUS_INVALID_REQUEST    = 'INVALID_REQUEST';
    const STATUS_UNKNOWN_ERROR      = 'UNKNOWN_ERROR';
    const STATUS_RESPONSE_ERROR     = 'RESPONSE_ERROR';
    const STATUS_REQUEST_DENIED     = 'REQUEST_DENIED';

    /** @var Client */
    private $client;

    /** @var string  */
    protected $apiKey;

    /** @var string  */
    protected $lang = 'en';

    /** @var string  */
    protected $base_uri;

    /** @var string */
    protected $url;

    /** @var float  */
    protected $timeout  = 2.0;

    /**
     * GoogleClientAbstract constructor.
     * @param null $apiKey
     * @param null $lang
     */
    public function __construct($apiKey = null, $lang = null)
    {
        $this->apiKey = $apiKey;
        $this->lang   = $lang;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;

        return $this;
    }

    /**
     * @return string
     */
    public function setLang()
    {
        return $this->lang;
    }

    public function getLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    private function getClient()
    {
        if($this->client === null){

            $this->client = new Client([
                'base_uri'          => $this->base_uri,
                'read_timeout'      => $this->timeout,
                'connect_timeout'   => $this->timeout,
            ]);
        }

        return $this->client;
    }

    public function call(Array $params = [] )
    {
        $output   = null;
        $params   = $this->getQuery($params);
        $response = $this->getClient()->request('GET', $this->base_uri, [
            'query' => $params,
        ]);

    //    dump($params); exit;

        if($response->getStatusCode() === 200){

            $output = json_decode($response->getBody()->getContents());

            if($output->status !== static::STATUS_OK){

                $this->throwErrorResponse($output);
            }
        }

        return $output;
    }

    protected function getQuery(Array $params = []) : array
    {
        return array_merge( [
            'key'       => $this->apiKey,
            'language'  => $this->lang,
            'timestamp' => time(),
        ], $params);
    }

    public function throwErrorResponse($response)
    {
        switch($response->status){

            case static::STATUS_ZERO_RESULTS:     throw new \Backtheweb\Google\Exception\ZeroResults();     break;
            case static::STATUS_OVER_QUERY_LIMIT: throw new \Backtheweb\Google\Exception\OverQueryLimit();  break;
            case static::STATUS_INVALID_REQUEST:  throw new \Backtheweb\Google\Exception\InvalidRequest();  break;
            case static::STATUS_UNKNOWN_ERROR:    throw new \Backtheweb\Google\Exception\UnknowError();     break;
            case static::STATUS_RESPONSE_ERROR:   throw new \Backtheweb\Google\Exception\ResponseError();   break;
            case static::STATUS_REQUEST_DENIED:   throw new \Backtheweb\Google\Exception\RequestDenied();   break;
        }

        throw new \Backtheweb\Google\Exception($response->message);
    }

}