<?php

namespace Backtheweb\Google\Contract;

class ModelContract {

    function toCamelCase($value){
        $value = ucwords(str_replace(array('-', '_'), ' ', $value));
        $value = str_replace(' ', '', $value);
        return lcfirst($value);
    }


    public function __construct( $options = [])
    {
        $this->setOptions($options);
    }


    /**
     * @param array $options
     * @return $this
     */
    public function setOptions($options) {

        $class_methods = get_class_methods($this);

        foreach ($options as $key => $value) {
            $method = 'set' . ucfirst($this->toCamelCase($key));
            if(in_array($method, $class_methods)){
                if($value instanceof \stdClass){
                    $obj = '\\Backtheweb\\Google\\Model\\' . ucfirst($key);
                    $this->$method(new $obj($value));
                } else {
                    $this->$method($value);
                }
            }
        }

        return $this;
    }

    /**
     * @param string $name
     * @param string $value
     * @throws \Exception
     */
    public function __set($name, $value)
    {
        $method = 'set' . ucfirst( $this->toCamelCase($name) );

        if (!method_exists ( $this, $method )) {
            throw new \Exception ( sprintf('Invalid property `%s` on `%s`', $name, get_class($this)) );
        }

        $this->$method ( $value );
    }

    /**
     * @param string $name
     * @return mixed
     * @throws \Exception
     */
    public function __get($name)
    {
        $method = 'get' . ucfirst( $this->toCamelCase($name) );
        if (!method_exists ( $this, $method )) {
            throw new \Exception ( sprintf('Invalid property `%s` on `%s`', $name, get_class($this)) );
        }
        return $this->$method ();
    }
}