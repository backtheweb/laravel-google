<?php

namespace Backtheweb\Google\Facade;

class Distance extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'backtheweb.google.distance';
    }
}