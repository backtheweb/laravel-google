<?php

namespace Backtheweb\Google\Facade;

class Timezone extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'backtheweb.google.timezone';
    }
}