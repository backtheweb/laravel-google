<?php

namespace Backtheweb\Google\Facade;

class TagManager extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'backtheweb.google.tagmanager';
    }
}