<?php

namespace Backtheweb\Google\Facade;

class Places extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'backtheweb.google.places';
    }
}