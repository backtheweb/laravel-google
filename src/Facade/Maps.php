<?php

namespace Backtheweb\Google\Facade;

class Maps extends \Illuminate\Support\Facades\Facade
{
    /**
     * {@inheritDoc}
     */
    protected static function getFacadeAccessor()
    {
        return 'backtheweb.google.maps';
    }
}