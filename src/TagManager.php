<?php

namespace Backtheweb\Google;

use Backtheweb\Google\TagManager\DataLayer;
use Illuminate\Support\Collection;

class TagManager
{
    /**
     * @var string
     */
    public $id;

    /**
     * @var bool
     */
    public $enabled;

    /**
     * @var DataLayer
     */
    protected $dataLayer;

    /**
     * TagManager constructor.
     * @param $id
     * @param array $dataLayer
     */
    public function __construct($id, Array $dataLayer = [], $enabled = true)
    {
        $this->id             = $id;
        $this->dataLayer      = new DataLayer($dataLayer);
        $this->enabled        = $enabled;
    }

    public function id()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Enable Google Tag Manager scripts rendering.
     */
    public function enable()
    {
        $this->enabled = true;
    }
    /**
     * Disable Google Tag Manager scripts rendering.
     */
    public function disable()
    {
        $this->enabled = false;
    }
    /**
     * Check whether script rendering is enabled.
     *
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * Push data to datalayer
     *
     * @param $key
     * @param null $value
     * @return $this
     */
    public function push($key, $value = null)
    {
        $this->dataLayer->push($key, $value);

        return $this;
    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function get($key){

        return $this->dataLayer->get($key);
    }

    /**
     * @return DataLayer
     */
    public function getDataLayer()
    {
        return $this->dataLayer;
    }

    /**
     * Clear the data layer.
     */
    public function clear()
    {
        $this->dataLayer = new DataLayer();
    }

    /*** DL seters and getters ***/

    /**
     * @param $value
     * @return $this
     */
    public function setDomain($value)
    {
        $this->push('domain', $value);
        return $this;
    }

    public function __call($key, array $arguments)
    {
        return $this->dataLayer->get($key);
    }

    public function query(array $data = [])
    {
        return http_build_query( array_merge( $this->dataLayer->toArray(), $data ) );
    }
}