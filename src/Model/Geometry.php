<?php

namespace Backtheweb\Google\Model;

use Backtheweb\Google\Contract\ModelContract;

class Geometry extends ModelContract
{

    const LOCATION_TYPE_APPROXIMATE = 'APPROXIMATE';

    /** @var Bounds */
    protected $bounds;

    /** @var Location */
    protected $location;

    /** @var string */
    protected $location_type = Geometry::LOCATION_TYPE_APPROXIMATE;

    /** @var Viewport */
    protected $viewport;

    /**
     * @return Bounds
     */
    public function getBounds(): Bounds
    {
        return $this->bounds;
    }

    /**
     * @param Bounds $bounds
     */
    public function setBounds(Bounds $bounds)
    {
        $this->bounds = $bounds;
    }

    /**
     * @return Location
     */
    public function getLocation(): Location
    {
        return $this->location;
    }

    /**
     * @param Location $location
     */
    public function setLocation(Location $location)
    {
        $this->location = $location;
    }

    /**
     * @return string
     */
    public function getLocationType(): string
    {
        return $this->location_type;
    }

    /**
     * @param string $location_type
     */
    public function setLocationType(string $location_type)
    {
        $this->location_type = $location_type;
    }

    /**
     * @return Bounds
     */
    public function getViewport(): Bounds
    {
        return $this->viewport;
    }

    /**
     * @param Bounds $viewport
     */
    public function setViewport(Bounds $viewport)
    {
        $this->viewport = $viewport;
    }
}