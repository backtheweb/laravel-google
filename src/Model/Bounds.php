<?php
namespace Backtheweb\Google\Model;

use Backtheweb\Google\Contract\ModelContract;

class Bounds extends ModelContract
{

    /** @var Location */
    protected $northeast;

    /** @var Location */
    protected $southwest;

    /**
     * @return Location
     */
    public function getNortheast(): Location
    {
        return $this->northeast;
    }

    /**
     * @param Location $northeast
     */
    public function setNortheast(Location $northeast)
    {
        $this->northeast = $northeast;
    }

    /**
     * @return Location
     */
    public function getSouthwest(): Location
    {
        return $this->southwest;
    }

    /**
     * @param Location $southwest
     */
    public function setSouthwest(Location $southwest)
    {
        $this->southwest = $southwest;
    }


}