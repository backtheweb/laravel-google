<?php

namespace Backtheweb\Google\Model;

use Backtheweb\Google\Contract\ModelContract;

class GeoCodeAddress extends ModelContract
{
    /** @var AddressComponent[]  */
    protected $address_components = [];

    /** @var string */
    protected $formatted_address;

    /** @var Geometry */
    protected $geometry;

    /** @var array  */
    protected $types = [];

    /** @var string */
    protected $place_id;

    /** @var Timezone */
    protected $timezone;

    /**
     * @return AddressComponent[]
     */
    public function getAddressComponents(): array
    {
        return $this->address_components;
    }

    /**
     * @param AddressComponent[] $components
     */
    public function setAddressComponents(array $components)
    {

        $this->address_components = [];

        foreach ($components as $c){

            if(!is_array($c)) {
                $c = new AddressComponent($c);
            }

            $this->addAddressComponent($c);
        }
    }

    public function addAddressComponent(AddressComponent $component)
    {
        $this->address_components[] = $component;
    }

    /**
     * @return string
     */
    public function getFormattedAddress(): string
    {
        return $this->formatted_address;
    }

    /**
     * @param string $formatted_address
     */
    public function setFormattedAddress(string $formatted_address)
    {
        $this->formatted_address = $formatted_address;
    }

    /**
     * @return Geometry
     */
    public function getGeometry(): Geometry
    {
        return $this->geometry;
    }

    /**
     * @param Geometry $geometry
     */
    public function setGeometry(Geometry $geometry)
    {
        $this->geometry = $geometry;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    /**
     * @param array $types
     */
    public function setTypes(array $types)
    {
        $this->types = $types;
    }

    /**
     * @return string
     */
    public function getPlaceId(): string
    {
        return $this->place_id;
    }

    /**
     * @param string $place_id
     */
    public function setPlaceId(string $place_id)
    {
        $this->place_id = $place_id;
    }

    public function getName()
    {
        return isset($this->address_components[0]) ? $this->address_components[0]->long_name : null;
    }

    public function getShortName()
    {
        return isset($this->address_components[0]) ? $this->address_components[0]->short_name : null;
    }

    public function getLocation()
    {
        return $this->getGeometry() ? $this->getGeometry()->getLocation() : null;
    }

    public function getLatLng()
    {
        return $this->getLocation() ? $this->getLocation()->getLatLng() : null;
    }

    public function isCity()
    {
        return in_array(AddressComponent::TYPE_LOCALITY, $this->getTypes() );
    }

    public function isAdministrationArea()
    {
        $levels = [
            AddressComponent::TYPE_ADMINISTRATIVE_AREA_LEVEL_1,
            AddressComponent::TYPE_ADMINISTRATIVE_AREA_LEVEL_2,
            AddressComponent::TYPE_ADMINISTRATIVE_AREA_LEVEL_3,
            AddressComponent::TYPE_ADMINISTRATIVE_AREA_LEVEL_4,
            AddressComponent::TYPE_ADMINISTRATIVE_AREA_LEVEL_5,
        ];

        foreach ($levels as $leve) {

            if(in_array($leve, $this->getTypes())){

                return true;
            };
        }

        return false;
    }

    public function isCountry()
    {
        return in_array(AddressComponent::TYPE_COUNTRY, $this->getTypes() );
    }

    public function isRegion()
    {
        return in_array(AddressComponent::TYPE_ADMINISTRATIVE_AREA_LEVEL_1, $this->getTypes() );
    }

    public function getAddressComponentByType($type)
    {

        foreach($this->getAddressComponents() as $component)
        {
            if( in_array($type, $component->getTypes())){

                return $component;
            }
        }

        return null;
    }

    public function getTimezone()
    {

        if(!$this->timezone && $this->getLocation() ){

            $this->timezone = \Timezone::getByLocation($this->getLocation());
        }

        return $this->timezone;
    }

    public function getTimezoneId()
    {
        return $this->getTimezone() ? $this->getTimezone()->getTimeZoneId() : null;
    }

}