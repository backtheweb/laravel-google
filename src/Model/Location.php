<?php

namespace Backtheweb\Google\Model;

use Backtheweb\Google\Contract\ModelContract;

class Location extends ModelContract
{
    /** @var float  */
    protected $lat = .0;

    /** @var float  */
    protected $lng = .0;

    /**
     * @return float
     */
    public function getLat(): float
    {
        return $this->lat;
    }

    /**
     * @param float $lat
     */
    public function setLat(float $lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return float
     */
    public function getLng(): float
    {
        return $this->lng;
    }

    /**
     * @param float $lng
     */
    public function setLng(float $lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getLat() . ',' . $this->getLng();
    }

    /**
     * @return object
     */
    public function getLatLng()
    {
        return (object) [
          'lat' => $this->getLat(),
          'lng' => $this->getLng(),
        ];
    }

}