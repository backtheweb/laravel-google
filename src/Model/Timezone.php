<?php

namespace Backtheweb\Google\Model;


use Backtheweb\Google\Contract\ModelContract;

class Timezone extends ModelContract
{

    /** @var Int */
    protected $dstOffset;

    /** @var Int */
    protected $rawOffset;

    /** @var string */
    protected $timeZoneId;

    /** @var string */
    protected $timeZoneName;

    /**
     * @return Int
     */
    public function getDstOffset(): Int
    {
        return $this->dstOffset;
    }

    /**
     * @param Int $dstOffset
     */
    public function setDstOffset(Int $dstOffset)
    {
        $this->dstOffset = $dstOffset;
    }

    /**
     * @return Int
     */
    public function getRawOffset(): Int
    {
        return $this->rawOffset;
    }

    /**
     * @param Int $rawOffset
     */
    public function setRawOffset(Int $rawOffset)
    {
        $this->rawOffset = $rawOffset;
    }

    /**
     * @return string
     */
    public function getTimeZoneId()
    {
        return $this->timeZoneId;
    }

    /**
     * @param string $timeZoneId
     */
    public function setTimeZoneId(string $timeZoneId)
    {
        $this->timeZoneId = $timeZoneId;
    }

    /**
     * @return string
     */
    public function getTimeZoneName(): ?string
    {
        return $this->timeZoneName;
    }

    /**
     * @param string $timeZoneName
     */
    public function setTimeZoneName(string $timeZoneName)
    {
        $this->timeZoneName = $timeZoneName;
    }

}