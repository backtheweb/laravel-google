<?php

namespace Backtheweb\Google\Model;


use Backtheweb\Google\Contract\ModelContract;

class AddressComponent extends ModelContract
{

    const TYPE_STREET_ADDRESS              = 'street_address';
    const TYPE_ROUTE                       = 'route';
    const TYPE_INTERSECTION                = 'intersection';
    const TYPE_POLITICAL                   = 'political';
    const TYPE_COUNTRY                     = 'country';
    const TYPE_ADMINISTRATIVE_AREA_LEVEL_1 = 'administrative_area_level_1';
    const TYPE_ADMINISTRATIVE_AREA_LEVEL_2 = 'administrative_area_level_2';
    const TYPE_ADMINISTRATIVE_AREA_LEVEL_3 = 'administrative_area_level_3';
    const TYPE_ADMINISTRATIVE_AREA_LEVEL_4 = 'administrative_area_level_4';
    const TYPE_ADMINISTRATIVE_AREA_LEVEL_5 = 'administrative_area_level_5';
    const TYPE_COLLOQUIAL_AREA             = 'colloquial_area';
    const TYPE_LOCALITY                    = 'locality';
    const TYPE_WARD                        = 'ward';
    const TYPE_SUBLOCALITY                 = 'sublocality';
    const TYPE_NEIGHBORHOOD                = 'neighborhood';
    const TYPE_PREMISE                     = 'premise';
    const TYPE_SUBPREMISE                  = 'subpremise';
    const TYPE_POSTAL_CODE                 = 'postal_code';
    const TYPE_NATURAL_FEATURE             = 'natural_feature';
    const TYPE_AIRPORT                     = 'airport';
    const TYPE_PARK                        = 'park';
    const TYPE_POINT_OF_INTEREST           = 'point_of_interest';
    const TYPE_FLOOR                       = 'floor';
    const TYPE_ESTABLISHMENT               = 'establishment';
    const TYPE_PARKING                     = 'parking';
    const TYPE_POSTAL_TOWN                 = 'postal_town';
    const TYPE_STREET_NUMBER               = 'street_number';
    const TYPE_BUS_STATION                 = 'bus_station';
    const TYPE_TRAIN_STATION               = 'train_station';
    const TYPE_TRANSIT_STATION             = 'transit_station';



    /** @var string */
    protected $long_name;

    /** @var string */
    protected $short_name;

    /** @var array  */
    protected $types      = [];

    /**
     * @return string
     */
    public function getLongName(): string
    {
        return $this->long_name;
    }

    /**
     * @param string $long_name
     */
    public function setLongName(string $long_name)
    {
        $this->long_name = $long_name;
    }

    /**
     * @return string
     */
    public function getShortName(): string
    {
        return $this->short_name;
    }

    /**
     * @param string $short_name
     */
    public function setShortName(string $short_name)
    {
        $this->short_name = $short_name;
    }

    /**
     * @return array
     */
    public function getTypes(): array
    {
        return $this->types;
    }

    public function getType()
    {
        return isset($this->types[0]) ? $this->types[0] : null;
    }

    /**
     * @param array $types
     */
    public function setTypes(array $types)
    {
        $this->types = $types;
    }
}