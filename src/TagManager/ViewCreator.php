<?php

namespace Backtheweb\Google\TagManager;

use Backtheweb\Google\TagManager as Manager;
use Illuminate\View\View;

class ViewCreator
{
    /**
     * @var Manager
     */
    protected $tagManager;

    public function __construct(Manager $tagManager)
    {
        $this->tagManager = $tagManager;
    }

    public function create(View $view)
    {
        if ($this->tagManager->enabled && empty($this->tagManager->id )) {
            //throw new ApiKeyNotSetException();
        }

        $view
            ->with('id',        $this->tagManager->id)
            ->with('enabled',   $this->tagManager->enabled)
            ->with('dataLayer', $this->tagManager->getDataLayer())
        ;

    }
}