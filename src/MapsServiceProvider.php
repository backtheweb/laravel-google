<?php

namespace Backtheweb\Google;

use Illuminate\Support\ServiceProvider;

/**
 *
 *
 * php artisan vendor:publish --provider="Backtheweb\Google\MapsServiceProvider" --tag="config"
 *
 * Class BackthewebServiceProvider
 * @package Backtheweb\Geo
 */

class MapsServiceProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([__DIR__.'/../config/maps.php' => config_path('maps.php')], 'config');
    }

    /**
     * Register the application services.
     *s
     * @return void
     */
    public function register()
    {
        $this->app->singleton('backtheweb.google.maps',function($app) {
            $lang = \Locale::getPrimaryLanguage(\App::getLocale());
            $key  = $this->app['config']->get('services.google.keys.0');
            return new Maps\Maps($key, $lang);
        });

        /*
        $this->app->singleton('backtheweb.google.timezone', function($app) {
            $lang = \Locale::getPrimaryLanguage(\App::getLocale());
            $key  = $app['config']->get('services.google.keys.0');
            return new Service\Timezone($key, $lang);
        });
        */
        /*
        $this->app->singleton('backtheweb.google.distance', function($app) {
            $lang = \Locale::getPrimaryLanguage(\App::getLocale());
            $key  = $app['config']->get('services.google.keys.0');
            return new Service\Distance($key, $lang);
        });
        */
    }

    public function provides()
    {
        return  [
            'backtheweb.google.maps',
           // 'backtheweb.google.timezone',
           // 'backtheweb.google.distance',
        ];
    }
}
