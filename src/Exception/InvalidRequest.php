<?php

namespace Backtheweb\Google\Exception;

use Backtheweb\Google\Exception;

class InvalidRequest extends Exception
{
    protected $message = 'neventum google maps api: Invalid request';
}
