<?php

namespace Backtheweb\Google\Exception;

use Backtheweb\Google\Exception;

class RequestDenied extends Exception
{
    protected $message = 'neventum google maps api: The provided API key is invalid';
}
