<?php

namespace Backtheweb\Google\Exception;

use Backtheweb\Google\Exception;

class OverQueryLimit extends Exception
{
    protected $message = 'neventum google maps api: Over query limit';
}
