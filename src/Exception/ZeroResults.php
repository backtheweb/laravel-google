<?php

namespace Backtheweb\Google\Exception;

use Backtheweb\Google\Exception;

class ZeroResults extends Exception
{
    protected $message = 'neventum google maps api: zero results';
}
