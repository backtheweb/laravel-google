<?php

namespace Backtheweb\Google\Exception;

use Backtheweb\Google\Exception;

class ResponseError extends Exception
{
    protected $message = 'neventum google maps api: Response error';
}
