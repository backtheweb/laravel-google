<?php

namespace Backtheweb\Google;

use Illuminate\Support\ServiceProvider;

/**
 *
 *
 * php artisan vendor:publish --provider="Backtheweb\Google\TagManagerServiceProvider" --tag="config"
 *
 * Class BackthewebServiceProvider
 * @package Backtheweb\Geo
 */

class TagManagerServiceProvider extends ServiceProvider
{
    protected $defer = false;

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([         __DIR__ . '/../config/tagmanager.php'   => config_path('tagmanager.php')],                           'config');
        $this->publishes([         __DIR__ . '/resources/views/tagManager' => base_path('resources/views/vendor/tagManager') ],   'views');
        $this->loadViewsFrom(__DIR__ . '/resources/views/tagManager', 'tagmanager');

        $this->app['view']->creator([

                'tagmanager::head',
                'tagmanager::noscript',
            ],

            'Backtheweb\Google\TagManager\ViewCreator'
        );
    }

    /**
     * Register the application services.
     *s
     * @return void
     */
    public function register()
    {
        $id         = config('tagmanager.id');
        $dataLayer  = config('tagmanager.dataLayer');
        $enabled    = config('tagmanager.enabled', true);
        $tagManager = new TagManager($id, $dataLayer, $enabled);

        $this->app->instance('Backtheweb\Google\TagManager',       $tagManager);
        $this->app->alias(   'Backtheweb\Google\TagManager', 'backtheweb.google.tagmanager');
    }
}
