<?php

return  [
    'id'      => env('GOOGLE_TAG_MANAGER_ID'),
    'enabled' => env('GOOGLE_TAG_MANAGER_ENABLE', true),
    'dataLayer' => [
        'site'  => env('APP_NAME', 'laravel'),
        'debug' => env('APP_DEBUG', false),
    ],
];